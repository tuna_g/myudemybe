'use strict';
const { saltAndHashPassword } = require('../utils/password');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     console.log("Users")

     await queryInterface.bulkInsert("Users",
         [
             {
                 
                 firstName: "John",
                 lastName: "Doe",
                 email: "john@example.com",
                 password: await saltAndHashPassword("123456"),
                 createdAt: new Date(),
                 updatedAt: new Date(),
             },
             {
                    
                firstName: "Marry",
                lastName: "Jane",
                email: "mary@example.com",
                password: await saltAndHashPassword("123456"),
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                
                firstName: "Person",
                lastName: "PersonLast",
                email: "person@example.com",
                password: await saltAndHashPassword("123456"),
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            ],{});
        console.log("Roles")


            await queryInterface.bulkInsert( "Roles",
                [
                    {
                        
                        name: "Admin",
                        isActive: true,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        
                        name: "Instructor",
                        isActive: true,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        
                        name: "Student",
                        isActive: true,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        
                        name: "Default",
                        isActive: true,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    }
                ],
                {}
            );
            console.log("UserRoles")
    
            await queryInterface.bulkInsert("UserRoles",
                [
                    {
                        
                        userId: 1,
                        roleId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        
                        userId: 2,
                        roleId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
        
                        userId: 3,
                        roleId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                ],
                {}
            );
            console.log("Categories")
    
            await queryInterface.bulkInsert("Categories",
                [
                    {
                        // 1
                        name: "Development",
                        parentId: null,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 2
                        name: "Finance & Accounting",
                        parentId: null,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 3
                        name: "Photography & Video",
                        parentId: null,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {  
                        // 4
                        name: "Business",
                        parentId: null,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 5
                        name: "Web Development",
                        parentId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 6
                        name: "Data Science",
                        parentId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 7
                        name: "Mobile Development",
                        parentId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 8
                        name: "Accounting & Bookkeeping",
                        parentId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        //9
                        name: "Cryptocurrency & Blockchain",
                        parentId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 10
                        name: "Digital Photography",
                        parentId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 11
                        name: "Portrait Photography",
                        parentId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 12
                        name: "Entrepreneurship",
                        parentId: 4,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 13
                        name: "Sales",
                        parentId: 4,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 14   
                        name: "Project Management",
                        parentId: 4,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    }
                ],
                {}
            );
            console.log("Courses")
    
            await queryInterface.bulkInsert("Courses",
                [
                    {
                        // 1
                        // development finance photography business
                        name: "Javascript",
                        sku: "js",
                        description: "Javascript course for developers.",
                        price: 5,
                        instructorId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 2
                        name: "React",
                        sku: "rc",
                        description: "React course for developers.",
                        price: 5,
                        instructorId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 3
                        name: "Node.JS",
                        sku: "nodejs",
                        description: "NodeJs course for developers",
                        price: 5,
                        instructorId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 4
                        name: "HTML5",
                        sku: "html",
                        description: "HTML5 course for developers",
                        price: 5,
                        instructorId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 5
                        name: "Accounting",
                        sku: "acc",
                        description: "Accounting course.",
                        price: 5,
                        instructorId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 6
                        name: "Bookkeeping",
                        sku: "book",
                        description: "Bookkeeping course.",
                        price: 5,
                        instructorId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 7
                        name: "Financial accunting",
                        sku: "finacc",
                        description: "Financial accounting course.",
                        price: 5,
                        instructorId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 8
                        name: "Finance fundamentals",
                        sku: "finfun",
                        description: "Finance fundamentals course.",
                        price: 5,
                        instructorId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 9
                        name: "Photography fundamentals",
                        sku: "phofun",
                        description: "Photography fundamentals course.",
                        price: 5,
                        instructorId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 10
                        name: "Photoshop",
                        sku: "pshop",
                        description: "Photoshop course.",
                        price: 5,
                        instructorId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 11
                        name: "Night Photography",
                        sku: "nphoto",
                        description: "Night photography course.",
                        price: 5,
                        instructorId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 12
                        name: "Mobile Photography",
                        sku: "mphoto",
                        description: "Mobile photography course.",
                        price: 5,
                        instructorId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 13
                        name: "Business fundamentals",
                        sku: "bfun",
                        description: "Busines fundamentals course.",
                        price: 5,
                        instructorId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 14
                        name: "Entrepreneurship fundamentals",
                        sku: "enfun",
                        description: "Entrepreneurship fundamentals course.",
                        price: 5,
                        instructorId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 15
                        name: "Freelancing",
                        sku: "flancing",
                        description: "Freelancing course.",
                        price: 5,
                        instructorId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        // 16
                        name: "Startup",
                        sku: "sup",
                        description: "Startup course.",
                        price: 5,
                        instructorId: 3,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                ],
                {}
            );
    
            console.log("CourseCategories")
    
            await queryInterface.bulkInsert("CourseCategories",
            [
                {
                    
                    courseId: 1,
                    categoryId: 5,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 2,
                    categoryId: 5,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 3,
                    categoryId: 5,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 4,
                    categoryId: 5,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 5,
                    categoryId: 8,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 6,
                    categoryId: 8,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 7,
                    categoryId: 8,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 8,
                    categoryId: 8,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                
                    courseId: 9,
                    categoryId: 10,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 10,
                    categoryId: 10,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 11,
                    categoryId: 10,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 12,
                    categoryId: 10,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 13,
                    categoryId: 12,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 14,
                    categoryId: 12,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 15,
                    categoryId: 12,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    
                    courseId: 16,
                    categoryId: 12,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                }
            ],
            {}
            );
            console.log("UserCourses")
    
            await queryInterface.bulkInsert("UserCourses",
                [
                    {
                        
                        userId: 1,
                        courseId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
                        
                        userId: 2,
                        courseId: 1,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                    {
        
                        userId: 3,
                        courseId: 2,
                        createdAt: new Date(),
                        updatedAt: new Date(),
                    },
                ],
                {}
            );
    

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};

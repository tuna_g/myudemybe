'use strict';

//const User = require("./user");
//const Role = require("./role");

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserRole extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ User, Role }) {
      // define association here
      User.belongsToMany(Role, { through: UserRole, foreignKey:{allowNull:false, name:'userId'}});
      Role.belongsToMany(User, { through: UserRole, foreignKey:{allowNull:false, name:'roleId'}});

      UserRole.belongsTo(User);
      UserRole.belongsTo(Role);

      User.hasMany(UserRole, {foreingKey: { name: "userId", allowNull: false }, onDelete: "CASCADE" });
      Role.hasMany(UserRole, {foreingKey: { name: "roleId", allowNull: false }, onDelete: "CASCADE" });
    }
  }
  UserRole.init({
    userId: DataTypes.INTEGER,
    roleId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserRole',
  });
  return UserRole;
};
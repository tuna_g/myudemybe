'use strict';
/*
const { user } = require("./user");
const { course } = require("./course");
*/
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserCourse extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({User, Course}) {
      // define association here
      User.belongsToMany(Course, { through: UserCourse, foreignKey:{allowNull:false, name:'userId'}});
      Course.belongsToMany(User, { through: UserCourse, foreignKey:{allowNull:false, name:'courseId'}});
    }
  }
  UserCourse.init({
    userId: DataTypes.INTEGER,
    courseId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserCourse',
  });
  return UserCourse;
};
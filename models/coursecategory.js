'use strict';


const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CourseCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // https://stackoverflow.com/questions/65972228/how-to-associate-models-in-es6-syntax-using-sequelize-v6
    static associate(models) {
      // define association here
      /*
      console.log(Course);
      Course.belongsToMany(Category, { through: CourseCategory, foreignKey:{allowNull:false, name:'courseId'}});
      Category.belongsToMany(Course, { through: CourseCategory, foreignKey:{allowNull:false, name:'categoryId'}});
*/
      //this.belongsTo(course);
      //this.belongsTo(category);

      //course.hasMany(CourseCategory, {foreingKey: { name: "userId", allowNull: false }, onDelete: "CASCADE" });
      //category.hasMany(CourseCategory, {foreingKey: { name: "roleId", allowNull: false }, onDelete: "CASCADE" });

    }
  }
  CourseCategory.init({
    courseId: DataTypes.INTEGER,
    categoryId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'CourseCategory',
  });
  return CourseCategory;
};
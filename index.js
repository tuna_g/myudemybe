const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const routes = require("./routes");
const { Sequelize } = require('sequelize');
const sequelize = new Sequelize('postgres://postgres:tuna1998@localhost:5432/MyUdemy');

//Test DB Connection
sequelize.authenticate()
    .then(() => {
        console.log("Connection successful");
    })
    .catch((err) => {
        console.log("Error: ", err);
    });

//Middlewares
app.use(cors());
app.use(bodyParser.json());

//Routes
app.use("/auth", routes.auth);

app.listen(5000, () => {
    console.log(`Server running on port: http://localhost:5000`);
});

/*app.listen(process.env.PORT, () => {
    console.log(`Server running on port: http://localhost:${process.env.PORT}`);
});*/